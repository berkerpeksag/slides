# PyPy 101

.fx: splash

![PyPy](http://pypy.org/image/pypy-logo.png)

**Berker Peksağ**

*26 Mayıs 2012*

[http://berkerpeksag.github.com/slides/pypy-101-pyist/](http://berkerpeksag.github.com/slides/pypy-101-pyist/)

---

# Berker Peksağ

* [berkerpeksag.com](http://berkerpeksag.com)
* @[berkerpeksag](http://twitter.com/berkerpeksag)
* [github/berkerpeksag](http://github.com/berkerpeksag)
* [mozillians.org/berkerpeksag](https://mozillians.org/en-US/berkerpeksag)

---

# Başlamadan Önce

* Bazı yerlerde fikir sahibi olunması için PyPy kaynak kodundan ufak örnekler verdim. Kodun tamamını okumak isteyenler
  için bağlantılarını da ekledim.

* Mümkün olduğunda Türkçe kelimeler kullanmaya çalıştım ancak *port*, *virtual machine*, *backend* vb.
  gibi artık alıştığımız ve Türkçesi pek *tercih edilmeyen* kelimeleri olduğu gibi kullandım.

* [İmplementasyon Türkçe değil](http://implement.asyonturkcedegil.com/) ama *kulak aşinalığı* nedeniyle sunum
  boyunca bol bol kullandım.

## CPython == Python

* **CPython**, Python programlama dilinin **Guido van Rossum** tarafından `C` ile yazılan referans implementasyonu.

---

# The Zen of Python vs. PyPy

> If the implementation is hard to explain, it's a bad idea.

---

# The Zen of Python vs. PyPy

> If the implementation is hard to explain, it's a bad idea.

# Except PyPy![^1]

[^1]: Kenneth Reitz ve Benjamin Peterson'ın sunumlarından alıntı.

---

# PyPy Öncesi

* Diğer tüm dinamik ve yorumlanabilir programlama dilleri gibi Python'un da yumuşak karnı hızı.
* PyPy bu açığı kapatmak için geliştirilmiş ilk proje değil.
* İlk örnekler [**Psyco**](http://psyco.sourceforge.net/) ve
  [**Stackless Python**](http://www.stackless.com/) projeleri.
* Son olarak yine JIT derleyicisi çözümünü kullanan [**HotPy**](http://hotpy.org/) projesi duyuruldu.

---

# PyPy Öncesi

## Psyco

* [http://psyco.sourceforge.net/](http://psyco.sourceforge.net/)
* JIT derleyicisi
* C ile CPython eklentisi olarak yazıldı.
* Python 3 ve 64 bit desteği yok.
* Artık geliştirilmiyor.

---

# PyPy Öncesi

## Stackless Python

* [http://www.stackless.com/](http://www.stackless.com/)
* CPython'dan dallanmış bir proje.
* Microthread.
* Coroutine.
* Google tarafından desteklendi.
* İmplementasyonun karmaşıklığı nedeniyle CPython ile birleştirilmesinden vazgeçildi.
* Halen bir grup gönüllü tarafından geliştiriliyor.
* Python 3 desteği var.
* Microthread desteği daha sonra CPython'a C modülü olarak [**greenlet**](http://codespeak.net/py/0.9.2/greenlet.html)
  adıyla port edildi.

---

# PyPy Nedir

Temelde iki ana projeden oluşur:

1. **PyPy:** Python Virtual Machine.
2. **RPython:** Kendi VMlerinizi yazmak için bir dil.

---

# PyPy Nedir

## Ekip

* Çoğunluğu dinamik programlama dilleri implementasyonu ve JIT derleyicileri üzerine doktora veya doktora
  sonrası araştırma yapıyor.
* Geliştiricilerinden ikisi CPython projesinde de çekirdek/ana ekipte yer alıyor.

---

# PyPy Nedir

## İmplementasyon örnekleri

* [JavaScript](https://bitbucket.org/pypy/lang-js)
* [Haskell](https://bitbucket.org/cfbolz/haskell-python)
* [Scheme](https://bitbucket.org/pypy/lang-scheme)
* [Smalltalk](https://bitbucket.org/pypy/lang-smalltalk)
* [Converge](https://github.com/ltratt/converge)

---

# PyPy Nedir

## CPython ile farkları

* `_md5`, `_io`, `_os` gibi orijinal olarak C ile yazılmış CPython modülerinin tamamı kullanılabilir...
* ...ancak performans nedeniyle(*GIL!*) tüm C modülleri RPython ile sıfırdan yazıldı.
* Eğer CPython için C ile yazılmış bir modülünüz varsa PyPy üzerinde kullanmak için RPython ile sıfırdan yazmak
  daha performanslı olacaktır.

---

# RPython

* **Restricted Python**
* Bildiğimiz Python ile sözdizimsel olarak hiçbir farkı yok...
* **Static-typed** olması dışında! (CPython *duck-typed*)
* Ayrıca, `os`, `math` ve `time` modülleri haricinde CPython ile birlikte gelen modülleri kullanamazsınız(İstisna olarak
  C ile yazılan `_md5` gibi modüller gösterilebilir).

---

# RPython

## Yerleşik fonksiyonlar

* `int`, `float`, `str`, `ord`, `chr`, `range` vb. kullanılabilir.

**Örnek:** `int` fonksiyonunun RPython implementasyonu.

[`pypy/annotation/builtin.py`](https://bitbucket.org/pypy/pypy/src/default/pypy/annotation/builtin.py)

    !python
    def builtin_int(s_obj, s_base=None):
        if isinstance(s_obj, SomeInteger):
            assert (not s_obj.unsigned,
                    "instead of int(r_uint(x)),use intmask(r_uint(x))")
        assert (s_base is None or isinstance(s_base, SomeInteger)
                and s_obj.knowntype == str),
                "only int(v|string) or int(string,int) expected"
        if s_base is not None:
            args_s = [s_obj, s_base]
        else:
            args_s = [s_obj]
        nonneg = isinstance(s_obj, SomeInteger) and s_obj.nonneg
        return constpropagate(int, args_s, SomeInteger(nonneg=nonneg))

---

# RPython

* Garbage collector
* Veri yapıları: `dict`, `tuple`, `list` vb.
* RPython, yazdığınız RPython programını C'ye çevirir.
* VM yazdığınız programlama dili için JIT derleyicisi var.

---

# RPython

## Backendler

* **C:** Varsayılan olarak çeviri işlemi C backend'i ile yapılır.
* **JVM:** Büyük oranda Polonyalı bir öğrenci tarafından yazıldı, şu an aktif olarak geliştirilmiyor.
* **CLR:** PyPy geliştiricilerinden Antonio Cuni'nin doktora tezi olarak yazdığı backend. Yine pek aktif
  olarak geliştirildiği söylenemez.

---

# RPython

## JIT derleyicisi

* Hibrid bir teknoloji.
* JVM ile programlama dilleri için geliştirilen derleyicilerde kullanım oranı arttı.
* [SpiderMonkey](https://developer.mozilla.org/en/SpiderMonkey) ile birlikte dinamik programlama
  dillerinin implementasyonunda *yeni moda* oldu.

---

# RPython

## JIT derleyicisi

* Programlama dillerinde bulunan güzel özelliklerin çoğu gibi JIT derleyicisi ile ilgili ilk çalışmalar da,
  **LISP**'in *mucidi* **John McCarthy** tarafından yapıldı.
* *Yorumlanan programlarda*, program her çalıştığında tekrar tekrar makine koduna çevrilir.
* *Derlenen programlarda* ise, çalışma zamanından önce makine koduna çevrilir.
* Bir JIT derleyicisi, basitçe bu iki kavramın karışımdan oluşur.

---

# RPython

## Örnek

* RPython ile implemente edilen `_md5` modülünü kullanmak için yazılan bir wrapper.

[`pypy/module/_md5/interp_md5.py`](https://bitbucket.org/pypy/pypy/src/4a38b43757e3/pypy/module/_md5/interp_md5.py)

    !python
    from pypy.rlib import rmd5
    from pypy.interpreter.baseobjspace import Wrappable
    from pypy.interpreter.typedef import TypeDef
    from pypy.interpreter.gateway import interp2app, unwrap_spec


    class W_MD5(Wrappable, rmd5.RMD5):
        def __init__(self, space):
            self.space = space
            self._init()

    # ...

---

# RPython

## Örnek

* CPython için C ile yazılan `_md5` modülünün RPython ile implemente edilmiş versiyonu.

[`pypy/rlib/rmd5.py`](https://bitbucket.org/pypy/pypy/src/4a38b43757e3/pypy/rlib/rmd5.py)

    !python
    class RMD5(object):
        """RPython-level MD5 object.
        """
        _mixin_ = True        # for interp_md5.py

        def __init__(self, initialdata=''):
            self._init()
            self.update(initialdata)


        def _init(self):
            """Set this object to an initial empty state.
            """
            self.count = r_ulonglong(0)   # total number of bytes
            self.input = ""   # pending unprocessed data, < 64 bytes
            self.uintbuffer = [r_uint(0)] * 16

     # ...

---

# RPython

## Eksiler

* Belgeler epey detaylı ama düzensiz.
* Sanki dünyanın sonu gelmeden önce apar topar yedek alınmış gibi!

---

# RPython

## Eksiler (2)

* Virtual Machine her güncellendiğinde tüm programın en baştan çevirilmesi gerekiyor.
* PyPy VM'i için bu süre, iyi bir bilgisayarla **~40 dakika**!

---

# PyPy'ın Geleceği

Geliştirilmesi devam eden üç farklı büyük proje var:

1. NumPy
2. Python 3
3. Software Transactional Memory

---

# PyPy'ın Geleceği

## NumPy

* [http://buildbot.pypy.org/numpy-status/latest.html](http://buildbot.pypy.org/numpy-status/latest.html)
* En aktif PyPy projesi.
* ~50%'lik bölümü port edildi.

---

# PyPy'ın Geleceği

## Python 3

* [https://bitbucket.org/pypy/pypy/src/py3k](https://bitbucket.org/pypy/pypy/src/py3k)
* Çoğu yerleşik fonkisyon kaldırıldı ya da güncellendi.
* Tüm testler Python 3'e port ediliyor.
* `str` vs. `bytes` vs. `unicode`.
* Python 3 sözdiziminin implementasyonu(yeni *Metaclass* sözdizimi gibi) tamamlandı.

---

# PyPy'ın Geleceği

## Software Transactional Memory

* [https://bitbucket.org/pypy/pypy/src/stm-thread](https://bitbucket.org/pypy/pypy/src/stm-thread)
* **Global Interpreter Lock** yerine alternatif bir çözüm.
* Direkt hafızaya erişmek yerine yerel bir thread'de gerekli işlemleri yapar.

**Örnek:**

    !python
    def f(list1, list2):
        x = list1.pop()
        list2.append(x)

**STM ile**:

    !python
    def f(list1, list2):
        while True:
            t = transaction()
            x = list1.pop(t)
            list2.append(t, x)
            if t.commit():
                break

---

# PyPy'ın Geleceği

## Eksiler

* Pek çok senaryoda Python'a göre *~5.5 kat* hızlı
  ama halen PyPy bug tracker'ı *"bu kod PyPy'da CPython'dan
  daha yavaş çalışıyor"* gibi hata kayıtlarıyla dolu.
* [PyPy list element del insert too slow](https://bugs.pypy.org/issue1093)
* [Significantly slow joins](https://bugs.pypy.org/issue994)
* [PyPy is slower on longs than it should](https://bugs.pypy.org/issue892)

---

# PyPy'ın Geleceği

## Eksiler (2)

* Geliştirilmesi gönüllülük esasından ziyade bağışlara göre devam ediyor. Günün birinde bağışlar
  kesilirse projenin devam edip etmeyeceği meçhul.
* Yeni gelenlere karşı pek yardımsever değiller. CPython bu açıdan pek çok açık kaynak projeye
  örnek olmalı.

---

# Kaynaklar

* [PEP 20 -- The Zen of Python](http://www.python.org/dev/peps/pep-0020/)

## PyPy

* [http://pypy.org/](http://pypy.org/)
* [http://speed.pypy.org/](http://speed.pypy.org/)
* [http://doc.pypy.org/](http://doc.pypy.org/)
* [http://morepypy.blogspot.com/](http://morepypy.blogspot.com/)
* [http://bugs.pypy.org/](http://doc.pypy.org/)
* [Makaleler](http://doc.pypy.org/en/latest/extradoc.html)
